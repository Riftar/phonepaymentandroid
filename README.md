# Paystore App

Paystore is an application created to make it easier for Telkom customers to pay their telephone bills based on customer number.

# Features
  - Register and Login
  - Pay Telephone Bill
  - Secure OTP
  - Payment History
  - Upload Receipt Photo

# Screenshot!
![screenshot](./image/screenshot 1.png)
![screenshot](./image/screenshot 2.png)
![screenshot](./image/screenshot 3.png)

Created by Kelompok 4 DANApprentech Batch 2